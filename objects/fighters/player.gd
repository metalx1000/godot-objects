extends KinematicBody2D

export (int) var speed = 3200
export (int) var jump_speed = -400
export (int) var gravity = 1000


var score = 0
var state = "play"
var velocity = Vector2.ZERO

onready var sprite=$Sprite
onready var animator=$AnimationPlayer
onready var ray = $RayCast2D

func _ready():
	pass
	#set_color(.8)
	
func _physics_process(delta):
	get_input(delta)
	jumping()
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	
func set_score(value = 1):
	score+=value

func jumping():
	#check if player is off ground
	if !ray.is_colliding(): 
		animator.play("jump")

func set_color(v):
	# Duplicate the shader so that changing its param doesn't change it on any other sprites that also use the shader.
	# Generally done once in _ready()
	sprite.set_material(sprite.get_material().duplicate(true))

	# Offset sprite hue by a random value within specified limits.
	#var rand_hue = float(randi() % 3)/2.0/3.2
	sprite.material.set_shader_param("Shift_Hue", v)


func attack(obj):
	
	if obj.has_method("take_damage"):
		obj.take_damage(1)
		velocity.y = jump_speed / 2
		
func get_input(delta):
	velocity.x = 0
	
	if Input.is_action_pressed("ui_right"):
		sprite.flip_h = false
		animator.play("walk")
		velocity.x += speed * delta * 2
	if Input.is_action_pressed("ui_left"):
		sprite.flip_h = true
		animator.play("walk") 
		velocity.x -= speed * delta * 2
		
	if velocity.x < 30 && velocity.x > -30:
		animator.play("idle")
		
	if Input.is_action_just_pressed("ui_up"):
		if is_on_floor():
			velocity.y = jump_speed
