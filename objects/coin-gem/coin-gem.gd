extends Node2D

var dead = false

func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if body.is_in_group("players") && !dead:
		dead = true
		body.set_score(1)
		$snd.play()
		$Sprite.visible = false
		$AnimationPlayer.play("death")
		
func death():
	queue_free()
